#!/bin/bash

# Sets the context for native peer commands
function usage {
    echo "Usage: . ./set_peer_env.sh ORG_NAME"
    echo " Sets the organization context for native peer execution"
}

if [ "$1" == "" ]; then
    usage
    exit
fi

# export ORG_CONTEXT=$1 
# MSP_ID="$(tr '[:lower:]' '[:upper:]' <<< ${ORG_CONTEXT:0:1})${ORG_CONTEXT:1}"

# export ORG_NAME=$MSP_ID

# Added this Oct 22
export CORE_PEER_LOCALMSPID=$2"MSP"

# Logging specifications
export FABRIC_LOGGING_SPEC=INFO

# Location of the core.yaml
export FABRIC_CFG_PATH=/ws/financialserviceconsortium/config/bnb


# Address of the peer
export CORE_PEER_ADDRESS=bnb.$1.bt:7051

# Local MSP for the admin - Commands need to be executed as org admin
export CORE_PEER_MSPCONFIGPATH=/ws/financialserviceconsortium/config/crypto-config/peerOrganizations/$1.bt/users/Admin@$1.bt/msp

# Address of the orderer
export ORDERER_ADDRESS=orderer.org.bt:7050

export CORE_PEER_TLS_ENABLED=false

peer channel list



# peer channel create -c financialchannel -f ./config/financialchannel/financialchannel.tx --outputBlock ./config/financialchannel/financialchannel.block -o $ORDERER_ADDRESS
# peer channel join -b ./config/financialchannel/financialchannel.block -o $ORDERER_ADDRESS

# # set PATH so it includes HLF bin if it exists
# if [ -d "/ws/financialserviceconsortium/bin" ] ; then
#     PATH="/ws/financialserviceconsortium/bin:$PATH"
# fis